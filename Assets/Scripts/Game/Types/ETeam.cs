﻿namespace Game.Types
{
    public enum ETeam : byte
    {
        None,
        Team_1,
        Team_2,
        Team_3,
    }
}