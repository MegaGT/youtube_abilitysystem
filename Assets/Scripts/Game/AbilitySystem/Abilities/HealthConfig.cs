﻿using System.Collections;
using UnityEngine;

namespace Game.AbilitySystem.Abilities
{
    [CreateAssetMenu(menuName = "Game/Abilities/Health", fileName = "HealthConfig")]
    public class HealthConfig : AbilityConfig
    {
        [field: SerializeField] public float HealthCount { get; private set; }

        public override AbilityBuilder GetBuilder()
        {
            return new HealthBuilder(this);
        }
    }
}