﻿using Game.Entities;
using Logics.Health;
using UnityEngine;

namespace Game.AbilitySystem.Abilities
{
    public class HealthAbility : Ability
    {
        public float HealthCount { get; private set; }

        public HealthAbility(float healthCount)
        {
            HealthCount = healthCount;
        }

        public override void Added(Actor owner)
        {
            owner.Health.AddHealthRegeneration(HealthCount);
        }

        public override void Remove(Actor owner)
        {
            
        }

    }
}