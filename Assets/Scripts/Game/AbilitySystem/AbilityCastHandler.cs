﻿using Game.AbilitySystem.AbilityUI;
using Game.Entities;
using Services.InputHandler;
using Services.Locator;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.AbilitySystem
{
    public class AbilityCastHandler : MonoBehaviour, IInjectServices
    {
        [SerializeField] private AbilityStorage _abilityStorage;
        [SerializeField] private Actor _ownerActor;
        [SerializeField] private LayerMask _targetsLayer;

        private List<Ability> _abilities = new();
        private Ability _currentAbility;

        private Camera _camera;
        private PlayerInput _playerInput;
        private AbilityDisplaying _abilityDisplaying;

        public void Inject(IServiceLocator locator)
        {
            _camera = Camera.main;
            _playerInput = locator.GetService<PlayerInput>();
            _abilityDisplaying = locator.GetService<AbilityDisplaying>();

            _abilityStorage.Init();
            _abilities.AddRange(_abilityStorage.GetAbilities());

            _abilityDisplaying.Init(_abilityStorage.GetAbilities());
            _abilityDisplaying.OnClickAbility += OnClickAbilityButton;
        }

        public void OnClickAbilityButton(int abilityIndex)
        {
            _currentAbility?.CancelCast();

            switch (_abilities[abilityIndex].Status)
            {
                case EAbilityStatus.Ready:

                    _currentAbility = _abilities[abilityIndex];
                    _currentAbility.StartCast();

                    break;
                case EAbilityStatus.Cooldown:
                    break;
                case EAbilityStatus.NeedMana:
                    break;
            }
        }

        private void Update()
        {
            for (int i = 0; i < _abilities.Count; ++i)
            {
                _abilities[i].EventTick(Time.deltaTime);

                if (_playerInput.OnKeyDown(_abilities[i].HotKey))
                {
                    OnClickAbilityButton(i);
                }
            }

            if(_currentAbility != null) 
            {
                if(_playerInput.OnRightMouseDown())
                {
                    _currentAbility.CancelCast();
                    _currentAbility = null;
                    return;
                }

                Vector3 location = Vector3.zero;
                Actor target = null;

                Ray ray = _camera.ScreenPointToRay(_playerInput.MousePosition());
                RaycastHit[] hitResult = Physics.RaycastAll(ray, 500.0f, _targetsLayer);

                for (int i = 0; i < hitResult.Length; ++i)
                {
                    hitResult[i].collider.TryGetComponent<Actor>(out target);

                    if (hitResult[i].collider.CompareTag("Ground"))
                    {
                        location = hitResult[i].point;
                    }
                }

                if(_currentAbility.CheckCondition(_ownerActor, target, location))
                {
                    if(_playerInput.OnLeftMouseDown())
                    {
                        _currentAbility.ApplyCast();
                        _currentAbility = null;
                    }
                }

            }
        }

        private void OnDestroy()
        {
            if(_abilityDisplaying != null)
                _abilityDisplaying.OnClickAbility -= OnClickAbilityButton;
        }
    }
}