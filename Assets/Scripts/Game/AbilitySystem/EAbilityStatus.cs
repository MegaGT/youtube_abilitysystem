﻿using System.Collections;
using UnityEngine;

namespace Game.AbilitySystem
{
    public enum EAbilityStatus : byte
    {
        None,
        Ready,
        Cooldown,
        NeedMana
    }
}